![](http://www.fullstacktraining.com/images/FST_logo_opt.png)

#Introduction to AngularJS
A fully functioning sample Single Page application

## To run without Grunt

* Put into a local webserver
* Run `npm install` in your terminal
	* This npm task automatically runs `bower install` as well

* Head to the index.html in a browser

## To run with Grunt

* Install Grunt globally: `npm install -g grunt`
* Install the grunt CLI globally: `npm install -g grunt-cli`
* Run `npm install` in your terminal
* Execute `grunt`
	* This will automatically launch a browser and you should see the content of index.html

## To run all tests
* Install Karma CLI globally
`npm install -g karma-cli`
* Install Protractor globally `npm install -g protractor`
* Execute `webdriver-manager update`
* Execute `webdriver-manager start`
* Make sure the application is running on 'http://localhost:4000/'
* Execute `npm test`

	

### To run unit tests

* Install Karma CLI globally
`npm install -g karma-cli` 
* Run Karma `karma start`

### To run functional tests

* Install Protractor globally `npm install -g protractor`
* Execute `webdriver-manager update`
* Execute `webdriver-manager start`
* Make sure the application is running on 'http://localhost:4000/'
* Execute the functional test `protractor test/conf.js`