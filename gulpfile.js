var gulp       = require('gulp');
var jshint     = require('gulp-jshint');
var concat     = require('gulp-concat');
var uglify     = require('gulp-uglify');
var rename     = require('gulp-rename');
var ngAnnotate = require('gulp-ng-annotate');
var sourcemaps = require('gulp-sourcemaps');

gulp.task('lint', function() {
  gulp.src(['./app/**/*.js', '!./app/**/*.test.js', '!./app/app.min.js'])
  .pipe(jshint())
  .pipe(jshint.reporter('default'));
});

gulp.task('scripts', function() {
  gulp.src(['./app/**/*.js', '!./app/**/*.test.js', '!./app/app.min.js'])
  .pipe(sourcemaps.init({loadMaps: true}))
    .pipe(concat('./app.js'))
    .pipe(ngAnnotate({
      add: true
    }))
    .pipe(uglify({sourceRoot: '.', sourceMapIncludeSources: true}))
  .pipe(rename({suffix:'.min'}))
  .pipe(sourcemaps.write('.'))
  .pipe(gulp.dest('dist/js'));
});

gulp.task('default', ['lint', 'scripts']);
