describe('datafactory', function() {
  var httpBackend;
  var errors = 0;
  var resultSingle = {"found": true};
  var resultArray = [{},{},{}];
  console.error = function() {
    errors = errors + 1;
  };

  beforeEach(module('starwars'));

  beforeEach(inject(function(_datafactory_, _$httpBackend_){
    // The injector unwraps the underscores (_) from around the parameter names when matching
    datafactory = _datafactory_;
    httpBackend = _$httpBackend_;
  }));

  it('should have a method for retrieving all characters', function() {
    expect(datafactory.getAllCharacters()).toBeDefined();
  });

  it('should have a method for retrieving a single character', function() {
    expect(datafactory.getOneCharacter()).toBeDefined();
  });

  it('should have a method for retrieving all vehicles', function() {
    expect(datafactory.getAllVehicles()).toBeDefined();
  });

  it('should have a method for retrieving a single vehicle', function() {
    expect(datafactory.getOneVehicle()).toBeDefined();
  });

  describe('methods', function() {

    afterEach(function() {
      httpBackend.verifyNoOutstandingExpectation();
      httpBackend.verifyNoOutstandingRequest();
    });

    describe('getAllCharacters', function() {

      it('should make a request and return a result', function() {
        // set up some data for the http call to return and test later.
        var returnData = [{},{},{}];

        // expectGET to make sure this is called once.
        httpBackend.expectGET('data/characters/starwars-characters.json').respond(returnData);

        // make the call.
        var returnedPromise = datafactory.getAllCharacters();

        // set up a handler for the response, that will put the result
        // into a variable in this scope for you to test.
        var result;
        returnedPromise.then(function(response) {
          result = response;
        });

        // flush the backend to "execute" the request to do the expectedGET assertion.
        httpBackend.flush();

        // check the result.
        // (after Angular 1.2.5: be sure to use `toEqual` and not `toBe`
        // as the object will be a copy and not the same instance.)
        expect(result.length).toBe(3);

      });

      it('should add an error to the console on error', function() {
        errors = 0;

        httpBackend.when('GET', 'data/characters/starwars-characters.json').respond(401, '');

        // make the call.
        datafactory.getAllCharacters();

        // flush the backend to "execute" the request to do the expectedGET assertion.
        httpBackend.flush();

        // check the result.
        expect(errors).toEqual(1);

      });
    });

    describe('getOneCharacter', function() {

      it('should make a request and return a result', function() {
        // set up some data for the http call to return and test later.
        var returnData = {"found" : true};

        // expectGET to make sure this is called once.
        httpBackend.expectGET('data/characters/r2d2.json').respond(returnData);

        // make the call.
        var returnedPromise = datafactory.getOneCharacter('r2d2');

        // set up a handler for the response, that will put the result
        // into a variable in this scope for you to test.
        var result;
        returnedPromise.then(function(response) {
          result = response;
        });

        // flush the backend to "execute" the request to do the expectedGET assertion.
        httpBackend.flush();

        // check the result.
        // (after Angular 1.2.5: be sure to use `toEqual` and not `toBe`
        // as the object will be a copy and not the same instance.)
        expect(result).toEqual(returnData);
      });

      it('should add an error to the console on error', function() {
        errors = 0;

        httpBackend.when('GET', 'data/characters/r2d2.json').respond(401, '');

        // make the call.
        datafactory.getOneCharacter('r2d2');

        // flush the backend to "execute" the request to do the expectedGET assertion.
        httpBackend.flush();

        // check the result.
        expect(errors).toEqual(1);

      });
    });

    describe('getAllVehicles', function() {
      beforeEach(function() {
        requestHandler = httpBackend.whenGET('data/vehicles/starwars-vehicles.json').respond(resultArray);
      });


      it('should make a request and return a result', function() {

        // expectGET to make sure this is called once.
        httpBackend.expectGET('data/vehicles/starwars-vehicles.json');

        // make the call.
        var returnedPromise = datafactory.getAllVehicles();

        // set up a handler for the response, that will put the result
        // into a variable in this scope for you to test.
        var result;
        returnedPromise.then(function(response) {
          result = response;
        });

        // flush the backend to "execute" the request to do the expectedGET assertion.
        httpBackend.flush();

        // check the result.
        // (after Angular 1.2.5: be sure to use `toEqual` and not `toBe`
        // as the object will be a copy and not the same instance.)
        expect(result.length).toBe(3);
      });
    });

    describe('getOneVehicle', function() {
      beforeEach(function() {
        requestHandler = httpBackend.whenGET('data/vehicles/tiefighter.json').respond(resultSingle);
      });

      it('should make a request and return a result', function() {
        // expectGET to make sure this is called once.
        httpBackend.expectGET('data/vehicles/tiefighter.json');

        // make the call.
        var returnedPromise = datafactory.getOneVehicle('tiefighter');

        // set up a handler for the response, that will put the result
        // into a variable in this scope for you to test.
        var result;
        returnedPromise.then(function(response) {
          result = response;
        });

        // flush the backend to "execute" the request to do the expectedGET assertion.
        httpBackend.flush();

        expect(result).toEqual(resultSingle);
      });

      it('should add an error to the console on error', function() {
        errors = 0;

        requestHandler.respond(401, '');

        // make the call.
        datafactory.getOneVehicle('tiefighter');

        // flush the backend to "execute" the request to do the expectedGET assertion.
        httpBackend.flush();

        // check the result.
        expect(errors).toEqual(1);
      });
    });

  });




});
