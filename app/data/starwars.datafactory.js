(function() {
  'use strict';

  angular
    .module('starwars')
    .factory('datafactory', datafactory);

    function datafactory($http) {
      function getAllCharacters() {
        return $http
          .get('data/characters/starwars-characters.json')
          .then(complete)
          .catch(failed);
      }

      function getOneCharacter(name) {
        return $http
          .get('data/characters/' + name + '.json')
          .then(complete)
          .catch(failed);
      }

      function getAllVehicles() {
        return $http
        .get('data/vehicles/starwars-vehicles.json')
        .then(complete)
        .catch(failed);
      }

      function getOneVehicle(name) {
        return $http
        .get('data/vehicles/' + name + '.json')
        .then(complete)
        .catch(failed);
      }

      // helper function to handle the resolved promise
      function complete(response) {
        return response.data;
      }

      // helper function to handle the rejected promise
      function failed(error) {
        console.error(error.statusText);
      }

      return {
        getAllCharacters: getAllCharacters,
        getOneCharacter: getOneCharacter,
        getAllVehicles: getAllVehicles,
        getOneVehicle: getOneVehicle
      };
    }

    datafactory.$inject = ['$http'];

})();
