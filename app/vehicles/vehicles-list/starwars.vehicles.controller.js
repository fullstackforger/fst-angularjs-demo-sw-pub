(function() {
  'use strict';

  angular
    .module('starwars')
    .controller('Vehicles', Vehicles);

    function Vehicles(datafactory) {
      var vm = this;
      vm.vehicles = [];

      datafactory.getAllVehicles()
        .then(function(data) {
          vm.vehicles = data;
        });
    }

    Vehicles.$inject = ['datafactory'];

})();
