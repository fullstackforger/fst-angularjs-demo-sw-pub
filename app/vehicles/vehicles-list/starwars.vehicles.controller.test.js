describe('vehicles controller', function() {
  var controller;
  var scope;
  var vm;
  var returnedData = [{},{},{}];

  beforeEach(module('starwars'));

  beforeEach(module(function($provide) {
    //  Create a mocked factory to provide data to the controller test
    //  Use $q to mock the promises
    $provide.factory('datafactory', function($q) {
      var getAllVehicles = jasmine.createSpy('getAllVehicles').and.callFake(function() {
        var passPromise = true;
        if (passPromise) {
          return $q.when(returnedData);
        }
        else {
          return $q.reject('something went wrong');
        }
      });

      return {
        getAllVehicles: getAllVehicles
      };
    });
  }));

  beforeEach(inject(function(_$rootScope_, _$controller_){
    // The injector unwraps the underscores (_) from around the parameter names when matching
    scope = _$rootScope_.$new();
    controller = _$controller_;
    vm = controller("Vehicles", { $scope: scope });
    scope.$apply();
  }));

  it('should set the vehicle data from the promise returned by the factory', function() {
    expect(vm.vehicles).toBe(returnedData);
  });

});
