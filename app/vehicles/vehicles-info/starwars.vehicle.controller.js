(function() {
  'use strict';

  angular
    .module('starwars')
    .controller('Vehicle', Vehicle);

    function Vehicle($routeParams, datafactory) {
      var vm = this;
      vm.name = $routeParams.name;
      vm.vehicle = [];

      datafactory.getOneVehicle(vm.name)
        .then(function(data) {
          vm.vehicle = data;
      });
    }

    Vehicle.$inject = ['$routeParams', 'datafactory'];


})();
