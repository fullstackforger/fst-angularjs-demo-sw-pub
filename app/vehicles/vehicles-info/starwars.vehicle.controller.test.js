describe('vehicle controller', function() {
  beforeEach(module('starwars'));
  var controller;
  var vm;
  var scope;
  var q;
  var datafactory;
  var nameReceived;
  var name = "tiefighter";
  var vehicle = {
    "description" : "The TIE (Twin Ion Engines) fighter is the unforgettable symbol of the Imperial fleet. Carried aboard Star Destroyers and battle stations, TIE fighters are single-pilot vehicles designed for fast-paced dogfights with Rebel X-wings and other starfighters. The iconic TIE fighter led to other models in the TIE family including the dagger-shaped TIE Interceptor and the explosive-laden TIE bomber. The terrifying roar of a TIE's engines strikes fear into the hearts of all enemies of the Empire.",
    "image" : "data/vehicles/image/tiefighter.jpeg",
    "name" : "TIE Fighter"
  };

  beforeEach(module(function($provide) {
    //  Create a mocked factory to provide data to the controller test
    //  Use $q to mock the promises
    $provide.factory('datafactory', function($q) {
      var getOneVehicle = jasmine.createSpy('getOneVehicle').and.callFake(function(n) {
        var vehicleObj = vehicle;
        nameReceived = n;
        var passPromise = true;
        if (passPromise) {
          return $q.when(vehicleObj);
        }
        else {
          return $q.reject('something went wrong');
        }
      });

      return {
        getOneVehicle: getOneVehicle
      };
    });
  }));

  beforeEach(inject(function(_$rootScope_, _$controller_){
    // The injector unwraps the underscores (_) from around the parameter names when matching
    scope = _$rootScope_.$new();
    controller = _$controller_;
    vm = controller("Vehicle", {
      $scope: scope,
      $routeParams : { name : name }
    });
    // Must $apply() to fire the promise function
    scope.$apply();

  }));

  it('should get the name from the route parameter', function() {
    expect(vm.name).toEqual(name);
  });

  it('should call the factory passing the name', function() {
    expect(nameReceived).toEqual(name);
  });

  it('should set the data into vm.vehicle when promise resolves', function() {
    expect(vm.vehicle).toBe(vehicle);
  });

});
