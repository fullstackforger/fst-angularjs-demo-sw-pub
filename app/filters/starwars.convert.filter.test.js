describe('convert filter', function() {
  beforeEach(module('starwars'));

  beforeEach(inject(function(_convertFilter_){
    // The injector unwraps the underscores (_) from around the parameter names when matching
    convert = _convertFilter_;
  }));


  it('should convert uppercase letters to lower case', function() {
    expect(convert('ABCDE')).toEqual('abcde');
  });

  it('should remove spaces', function() {
    expect(convert('A B C D E')).toEqual('abcde');
  });

  it('should remove hyphens', function() {
    expect(convert('A-B-C-D-E')).toEqual('abcde');
  });

  it('should remove hyphens and spaces', function() {
    expect(convert('AB C D-E -F')).toEqual('abcdef');
  });

});
