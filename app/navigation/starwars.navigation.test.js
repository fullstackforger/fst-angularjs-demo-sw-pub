describe('navigation controller', function() {
  var $location,
      controller,
      scope,
      vm;
  
  beforeEach(module('starwars'));

  beforeEach(inject(function(_$rootScope_, _$controller_, _$location_){
    // The injector unwraps the underscores (_) from around the parameter names when matching
    scope = _$rootScope_.$new();
    controller = _$controller_;
    $location = _$location_;
    vm = controller("NavigationController", { $scope: scope });
  }));

  it('should return "active" when the URL matches the hash', function() {
    $location.path('/characters');
    scope.$apply();
    expect(vm.isActive('characters')).toEqual('active');
  });

  it('should return an empty string when the URL does not match the hash', function() {
    $location.path('/vehicles');
    scope.$apply();
    expect(vm.isActive('characters')).toEqual('');
  });

  it('should work when there is no hash (i.e. the homepage)', function() {
    $location.path('/');
    scope.$apply();
    expect(vm.isActive('')).toEqual('active');
    expect(vm.isActive('characters')).toEqual('');
  });

});