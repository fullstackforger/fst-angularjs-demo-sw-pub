describe('multidisplay directive', function() {
  beforeEach(module('starwars'));
  beforeEach(module('app/directives/multidisplay.html'));

  var element;
  var outerScope;
  var innerScope;

  beforeEach(inject(function($rootScope, $compile) {
    element = angular.element('<div multi-display name="TIE Fighter" image="data/vehicles/image/tiefighter.jpeg"></div>');

    outerScope = $rootScope;
    $compile(element)(outerScope);
    innerScope = element.isolateScope();
    outerScope.$digest();

  }));

  it('should display the name', function() {
    expect(element.find('h3').text()).toEqual('TIE Fighter');
  });

  it('should display an image', function() {
    expect(element.find('img')[0].src).toContain('data/vehicles/image/tiefighter.jpeg');
  });

  it('should generate a websafe link', function() {
    expect(element.find('a')[0].href).toContain('tiefighter');
  });

});
