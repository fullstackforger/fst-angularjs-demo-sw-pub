describe('singledisplay directive', function() {
  beforeEach(module('starwars'));
  beforeEach(module('app/directives/singledisplay.html'));

  var element;
  var outerScope;
  var innerScope;

  beforeEach(inject(function($rootScope, $compile) {
    element = angular.element('<div single-display name="AT-AT Walker" image="data/vehicles/image/atatwalker.jpeg" description="The All Terrain Armored Transport, or AT-AT walker, is a four-legged transport and combat vehicle used by the Imperial ground forces. Standing over 20 meters tall with blast-impervious armor plating, these massive constructs are used as much for psychological effect as they are for tactical effect."></div>');

    outerScope = $rootScope;
    $compile(element)(outerScope);

    innerScope = element.isolateScope();
    outerScope.$digest();

  }));

  it('should display the name in h1', function() {
    expect(element.find('h1').text()).toEqual('AT-AT Walker');
  });

  it('should display the name in panel h3', function() {
    expect(element.find('h3').text()).toEqual('AT-AT Walker');
  });

  it('should display a paragraph of text', function() {
    expect(element.find('p').text().length).toBeGreaterThan(100);
  });
  
});
