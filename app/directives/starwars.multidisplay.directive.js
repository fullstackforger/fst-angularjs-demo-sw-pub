(function() {
  'use strict';

  angular
    .module('starwars')
    .directive('multiDisplay', multiDisplay);

    function multiDisplay() {
      var directive = {
        restrict: 'A',
        replace: true,
        scope: {
          name: '@',
          description: '@',
          image: '@'
        },
        templateUrl: 'app/directives/multidisplay.html',
      };

      return directive;
    }
})();
