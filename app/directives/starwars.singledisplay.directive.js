(function() {
  'use strict';

  angular.module('starwars')
  .directive('singleDisplay', singleDisplay);

  function singleDisplay() {
    var directive = {
      restrict: 'A',
      replace: true,
      scope: {
        name: '@',
        description: '@',
        image: '@'
      },
      templateUrl: 'app/directives/singledisplay.html'
    };

    return directive;
  }
})();
