(function() {
  'use strict';

  angular
    .module('starwars', [
      'ngRoute'
    ])
    .config(config);

  //routing configuration
  function config($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'app/main.html'
      })
      .when('/characters', {
        templateUrl: 'app/characters/characters-list/characters.html',
        controller: 'Characters',
        controllerAs: 'vm'
      })
      .when('/characters/:name', {
        templateUrl: 'app/characters/characters-info/character.html',
        controller: 'Character',
        controllerAs: 'vm'
      })
      .when('/vehicles', {
        templateUrl: 'app/vehicles/vehicles-list/vehicles.html',
        controller: 'Vehicles',
        controllerAs: 'vm'
      })
      .when('/vehicles/:name', {
        templateUrl: 'app/vehicles/vehicles-info/vehicle.html',
        controller: 'Vehicle',
        controllerAs: 'vm'
      })
      .otherwise({
        redirectTo: '/'
      });
  }

})();
