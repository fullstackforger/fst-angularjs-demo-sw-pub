(function() {
  'use strict';

  angular
    .module('starwars')
    .controller('Character', Character);

    function Character($routeParams, datafactory) {
      var vm = this;
      // Changed to vm.name to enable unit testing
      // var name = $routeParams.name;
      vm.name = $routeParams.name;
      vm.character = {};

      datafactory.getOneCharacter(vm.name)
        .then(function(data) {
          vm.character = data;
      });
    }

    Character.$inject = ['$routeParams', 'datafactory'];

})();
