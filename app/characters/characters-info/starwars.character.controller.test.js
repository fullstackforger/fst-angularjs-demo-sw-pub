describe('character controller', function() {
  beforeEach(module('starwars'));
  var controller;
  var vm;
  var scope;
  var q;
  var datafactory;
  var nameReceived;
  var name = "r2d2";
  var character = {
    "id": 4,
    "name": "R2-D2",
    "role": "Astromech Droid",
    "species": "droid",
    "gender": "",
    "height": "1.09",
    "weapon":"Electro-shock prod",
    "homeworld": "Naboo",
    "image": "data/characters/image/r2d2.png",
    "alliance": "rebel",
    "bio": "A resourceful astromech droid, R2-D2 served Padmé Amidala, Anakin Skywalker and Luke Skywalker in turn, showing great bravery in rescuing his masters and their friends from many perils. A skilled starship mechanic and fighter pilot's assistant, he formed an unlikely but enduring friendship with the fussy protocol droid C-3PO.  R2-D2's mechanical body contains numerous arms tipped with all manner of useful tools, from welding torches to computer interfaces. Like other astromechs, his language is a cacophony of beeps and whistles understandable only by some other droids. But the spunky little droid's abundant personality generally makes his meaning plain enough."
  };

  beforeEach(module(function($provide) {
    //  Create a mocked factory to provide data to the controller test
    //  Use $q to mock the promises
    $provide.factory('datafactory', function($q) {
      var getOneCharacter = jasmine.createSpy('getOneCharacter').and.callFake(function(n) {
        var charObj = character;
        nameReceived = n;
        var passPromise = true;
        if (passPromise) {
          return $q.when(charObj);
        }
        else {
          return $q.reject('something went wrong');
        }
      });

      return {
        getOneCharacter: getOneCharacter
      };
    });
  }));

  beforeEach(inject(function(_$rootScope_, _$controller_){
    // The injector unwraps the underscores (_) from around the parameter names when matching
    scope = _$rootScope_.$new();
    controller = _$controller_;
    vm = controller("Character", {
      $scope: scope,
      $routeParams : { name : name }
    });
    // Must $apply() to fire the promise function
    scope.$apply();

  }));

  it('should get the name from the route parameter', function() {
    expect(vm.name).toEqual(name);
  });

  it('should call the factory passing the name', function() {
    expect(nameReceived).toEqual(name);
  });

  it('should set the data into vm.character when promise resolves', function() {
    expect(vm.character).toBe(character);
  });

});
