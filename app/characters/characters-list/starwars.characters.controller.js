(function() {
  'use strict';

  angular
    .module('starwars')
    .controller('Characters', Characters);

    function Characters(datafactory) {
      var vm = this;
      vm.search = [];
      vm.characters = [];

      vm.choices = ['name', 'role'];
      vm.option = 'name';
      vm.alliances = ['rebel', 'empire'];
      vm.order = {
        'sortedBy': 'name',
        'reverse': false
      };

      vm.reset = function() {
        vm.search = [];
      };

      datafactory.getAllCharacters()
        .then(function(data) {
          vm.characters = data;
        });
    }

    Characters.$inject = ['datafactory'];


})();
