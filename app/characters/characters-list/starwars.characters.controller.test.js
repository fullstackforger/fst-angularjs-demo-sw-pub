describe('characters controller', function() {
  var controller;
  var scope;
  var vm;
  var returnedData = [{},{},{}];

  beforeEach(module('starwars'));

  beforeEach(module(function($provide) {
    //  Create a mocked factory to provide data to the controller test
    //  Use $q to mock the promises
    $provide.factory('datafactory', function($q) {
      var getAllCharacters = jasmine.createSpy('getAllCharacters').and.callFake(function() {
        var passPromise = true;
        if (passPromise) {
          return $q.when(returnedData);
        }
        else {
          return $q.reject('something went wrong');
        }
      });

      return {
        getAllCharacters: getAllCharacters
      };
    });
  }));

  beforeEach(inject(function(_$rootScope_, _$controller_){
    // The injector unwraps the underscores (_) from around the parameter names when matching
    scope = _$rootScope_.$new();
    controller = _$controller_;
    vm = controller("Characters", { $scope: scope });
    scope.$apply();
  }));

  it('should set option to be name', function() {
    expect(vm.option).toBe("name");
  });

  it('should reset the search text when the option is changed', function() {
    vm.search = ['Some search text'];
    vm.reset();
    expect(vm.search.length).toBe(0);
  });

  it('should set the character data from the promise returned by the factory', function() {
    expect(vm.characters).toBe(returnedData);
  });

});
