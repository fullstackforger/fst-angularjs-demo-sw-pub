exports.config = {
  seleniumAddress: 'http://localhost:4444/wd/hub',
  specs: ['*.spec.js'],
  multiCapabilities: [
    {
      browserName: 'chrome',
      platform: 'ANY'
    },
    // {
    //   browserName: 'firefox',
    //   directConnect: true,
    //   platform: 'ANY'
    // },
    {
      browserName: 'phantomjs',
      'phantomjs.binary.path': require('phantomjs').path,
      'phantomjs.ghostdriver.cli.args': ['--loglevel=DEBUG'],
      platform: 'ANY'
    }
  ],
  jasmineNodeOpts: {
    // If true, display spec names.
    isVerbose: true
  }
};
