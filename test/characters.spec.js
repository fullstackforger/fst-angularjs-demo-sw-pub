describe('The characters page', function() {

  browser.driver.manage().window().setSize(1280, 1024);
  // Create spec-wide variables to be reused in different tests
  // The element calls are not activated until used in an 'it' block
  var activeNav = element(by.css('.nav .active a'));

  // Get the elements in the character ng-repeat
  var characters = element.all(by.repeater('character in filtered'));

  var characterTotal;

  it('should show the "characters" navigation tab as active', function() {

    browser.get('http://localhost:4000/#/characters');
    expect(activeNav.getText()).toEqual('Characters');

  });

  it('should show 14 characters', function() {

    // Save the intial count of characters to the spec-wide variable
    characterTotal = characters.count();

    expect(characterTotal).toBe(14);

  });

  it('should show fewer characters when searching', function() {

    // Find the search input box and enter a search term
    element(by.model('vm.search[vm.option]')).sendKeys("darth");

    // Check that there are less characters than we started with
    expect(characters.count()).toBeLessThan(characterTotal);

  });

  it('should show all characters when clearing search', function() {

    // Find the search box and clear the contents
    element(by.model('vm.search[vm.option]')).clear();

    // Check that the number of characters is the same as we started with
    expect(characters.count()).toBe(characterTotal);

  });

  it('should reverse the display order when the name button is clicked', function() {

    // Find all of the character images
    var images = element.all(by.css('.character-image'));

    // Get the 'alt' attribute of the first image
    var firstAlt = images.first().getAttribute('alt');

    // Find and click the name sorting button
    // Note: Will need to add a class to the button in the HTML
    element(by.css('.btn-sort-name')).click();

    // Get the 'alt' of the last image
    // Check that it matches what used to be the first
    expect(images.last().getAttribute('alt')).toEqual(firstAlt);

  });

});
