describe('The homepage', function() {

  browser.driver.manage().window().setSize(1280, 1024);
  
  it('should have a title of "Star Wars"', function() {
    browser.get('http://localhost:4000/#/');
    expect(browser.getTitle()).toEqual('Star Wars');
  });

  it('should show the "home" navigation tab as active', function() {
    expect(element(by.css('.nav .active a')).getText()).toEqual('Home');
  });

});
